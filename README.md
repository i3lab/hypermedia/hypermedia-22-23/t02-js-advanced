# T02 - JS Advanced

This is the repo for the webpage that was developed during the lecture on 16/03/2023


## Content

The project is the same as the one available in repository T00:

 - Four webpages (in the root folder):
	 - The homepage (index.html)
	 - The page containing the cards of the dogs (dogs.html)
	 - The about page (about.html)
	 - The contact page (contact.html)
 - CSS files (in folder assets/css):
	 - one for each webpage, using the same name
	 - one for the general CSS style shared between pages (general.css)
 - Images used in the webpages (in folder assets/img)

Additionally, a JS file has been added to manage the dynamic page for the dogs.
